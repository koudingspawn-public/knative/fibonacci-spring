package de.koudingspawn.knativeperformance;

import org.springframework.stereotype.Service;

@Service
public class FibonacciService {

    double fib2(int n) {
        double f[] = new double[2 + n];
        int i;

        f[0] = 0;
        f[1] = 1;

        for (i = 2; i <= n; i++) {
            f[i] = f[i - 1] + f[i - 2];
        }

        return f[n];

    }

}
