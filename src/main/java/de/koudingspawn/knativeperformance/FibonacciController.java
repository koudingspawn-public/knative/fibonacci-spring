package de.koudingspawn.knativeperformance;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FibonacciController {

    private final FibonacciService fibonacciService;

    public FibonacciController(FibonacciService fibonacciService) {
        this.fibonacciService = fibonacciService;
    }

    @GetMapping("/fibonacci/{n}")
    public String fibonacci(@PathVariable int n) {

        return "Fibonacci is " + fibonacciService.fib2(n);
    }

    @GetMapping("/health")
    public String health() {
        return "OK";
    }
}