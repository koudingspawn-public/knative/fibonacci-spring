package de.koudingspawn.knativeperformance;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KnativePerformanceApplication {

	public static void main(String[] args) {
		SpringApplication.run(KnativePerformanceApplication.class, args);
	}

}
